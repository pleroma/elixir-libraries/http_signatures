# Pleroma: A lightweight social networking server
# SPDX-FileCopyrightText: 2017-2019 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: LGPL-3.0-only

# Test data from https://tools.ietf.org/html/draft-cavage-http-signatures-08#appendix-C
defmodule HttpSignaturesTest do
  use ExUnit.Case

  @public_key hd(:public_key.pem_decode(File.read!("test/public.key")))
              |> :public_key.pem_entry_decode()

  @private_key hd(:public_key.pem_decode(File.read!("test/private.key")))
               |> :public_key.pem_entry_decode()

  @headers %{
    "(request-target)" => "post /foo?param=value&pet=dog",
    "host" => "example.com",
    "date" => "Thu, 05 Jan 2014 21:31:40 GMT",
    "content-type" => "application/json",
    "digest" => "SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE=",
    "content-length" => "18"
  }

  @default_signature """
  keyId="Test",algorithm="rsa-sha256",signature="jKyvPcxB4JbmYY4mByyBY7cZfNl4OW9HpFQlG7N4YcJPteKTu4MWCLyk+gIr0wDgqtLWf9NLpMAMimdfsH7FSWGfbMFSrsVTHNTk0rK3usrfFnti1dxsM4jl0kYJCKTGI/UWkqiaxwNiKqGcdlEDrTcUhhsFsOIo8VhddmZTZ8w="
  """

  @basic_signature """
  keyId="Test",algorithm="rsa-sha256",headers="(request-target) host date",signature="HUxc9BS3P/kPhSmJo+0pQ4IsCo007vkv6bUm4Qehrx+B1Eo4Mq5/6KylET72ZpMUS80XvjlOPjKzxfeTQj4DiKbAzwJAb4HX3qX6obQTa00/qPDXlMepD2JtTw33yNnm/0xV7fQuvILN/ys+378Ysi082+4xBQFwvhNvSoVsGv4="
  """

  @all_headers_signature """
  keyId="Test",algorithm="rsa-sha256",headers="(request-target) host date content-type digest content-length",signature="Ef7MlxLXoBovhil3AlyjtBwAL9g4TN3tibLj7uuNB3CROat/9KaeQ4hW2NiJ+pZ6HQEOx9vYZAyi+7cmIkmJszJCut5kQLAwuX+Ms/mUFvpKlSo9StS2bMXDBNjOh4Auj774GFj4gwjS+3NhFeoqyr/MuN6HsEnkvn6zdgfE2i0="
  """

  test "split up a signature" do
    expected = %{
      "keyId" => "Test",
      "algorithm" => "rsa-sha256",
      "signature" =>
        "jKyvPcxB4JbmYY4mByyBY7cZfNl4OW9HpFQlG7N4YcJPteKTu4MWCLyk+gIr0wDgqtLWf9NLpMAMimdfsH7FSWGfbMFSrsVTHNTk0rK3usrfFnti1dxsM4jl0kYJCKTGI/UWkqiaxwNiKqGcdlEDrTcUhhsFsOIo8VhddmZTZ8w=",
      "headers" => ["date"]
    }

    assert HTTPSignatures.split_signature(@default_signature) == expected
  end

  test "validates the default case" do
    signature = HTTPSignatures.split_signature(@default_signature)
    assert HTTPSignatures.validate(@headers, signature, @public_key)
  end

  test "validates the basic case" do
    signature = HTTPSignatures.split_signature(@basic_signature)
    assert HTTPSignatures.validate(@headers, signature, @public_key)
  end

  test "validates the all-headers case" do
    signature = HTTPSignatures.split_signature(@all_headers_signature)
    assert HTTPSignatures.validate(@headers, signature, @public_key)
  end

  test "it contructs a signing string" do
    expected = "date: Thu, 05 Jan 2014 21:31:40 GMT\ncontent-length: 18"
    assert expected == HTTPSignatures.build_signing_string(@headers, ["date", "content-length"])
  end

  test "it parses the http signature for a conn" do
    conn = %{
      req_headers: [
        {"host", "localtesting.pleroma.lol"},
        {"connection", "close"},
        {"content-length", "2316"},
        {"user-agent", "http.rb/2.2.2 (Mastodon/2.1.0.rc3; +http://mastodon.example.org/)"},
        {"date", "Sun, 10 Dec 2017 14:23:49 GMT"},
        {"digest", "SHA-256=x/bHADMW8qRrq2NdPb5P9fl0lYpKXXpe5h5maCIL0nM="},
        {"content-type", "application/activity+json"},
        {"(request-target)", "post /users/demiurge/inbox"},
        {"signature",
         "keyId=\"http://mastodon.example.org/users/admin#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) user-agent host date digest content-type\",signature=\"i0FQvr51sj9BoWAKydySUAO1RDxZmNY6g7M62IA7VesbRSdFZZj9/fZapLp6YSuvxUF0h80ZcBEq9GzUDY3Chi9lx6yjpUAS2eKb+Am/hY3aswhnAfYd6FmIdEHzsMrpdKIRqO+rpQ2tR05LwiGEHJPGS0p528NvyVxrxMT5H5yZS5RnxY5X2HmTKEgKYYcvujdv7JWvsfH88xeRS7Jlq5aDZkmXvqoR4wFyfgnwJMPLel8P/BUbn8BcXglH/cunR0LUP7sflTxEz+Rv5qg+9yB8zgBsB4C0233WpcJxjeD6Dkq0EcoJObBR56F8dcb7NQtUDu7x6xxzcgSd7dHm5w==\""}
      ]
    }

    result = HTTPSignatures.signature_for_conn(conn)
    assert result["keyId"] == "http://mastodon.example.org/users/admin#main-key"
  end

  test "it validates a conn" do
    public_key_pem =
      "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnGb42rPZIapY4Hfhxrgn\nxKVJczBkfDviCrrYaYjfGxawSw93dWTUlenCVTymJo8meBlFgIQ70ar4rUbzl6GX\nMYvRdku072d1WpglNHXkjKPkXQgngFDrh2sGKtNB/cEtJcAPRO8OiCgPFqRtMiNM\nc8VdPfPdZuHEIZsJ/aUM38EnqHi9YnVDQik2xxDe3wPghOhqjxUM6eLC9jrjI+7i\naIaEygUdyst9qVg8e2FGQlwAeS2Eh8ygCxn+bBlT5OyV59jSzbYfbhtF2qnWHtZy\nkL7KOOwhIfGs7O9SoR2ZVpTEQ4HthNzainIe/6iCR5HGrao/T8dygweXFYRv+k5A\nPQIDAQAB\n-----END PUBLIC KEY-----\n"

    [public_key] = :public_key.pem_decode(public_key_pem)

    public_key =
      public_key
      |> :public_key.pem_entry_decode()

    conn = %{
      req_headers: [
        {"host", "localtesting.pleroma.lol"},
        {"connection", "close"},
        {"content-length", "2316"},
        {"user-agent", "http.rb/2.2.2 (Mastodon/2.1.0.rc3; +http://mastodon.example.org/)"},
        {"date", "Sun, 10 Dec 2017 14:23:49 GMT"},
        {"digest", "SHA-256=x/bHADMW8qRrq2NdPb5P9fl0lYpKXXpe5h5maCIL0nM="},
        {"content-type", "application/activity+json"},
        {"(request-target)", "post /users/demiurge/inbox"},
        {"signature",
         "keyId=\"http://mastodon.example.org/users/admin#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) user-agent host date digest content-type\",signature=\"i0FQvr51sj9BoWAKydySUAO1RDxZmNY6g7M62IA7VesbRSdFZZj9/fZapLp6YSuvxUF0h80ZcBEq9GzUDY3Chi9lx6yjpUAS2eKb+Am/hY3aswhnAfYd6FmIdEHzsMrpdKIRqO+rpQ2tR05LwiGEHJPGS0p528NvyVxrxMT5H5yZS5RnxY5X2HmTKEgKYYcvujdv7JWvsfH88xeRS7Jlq5aDZkmXvqoR4wFyfgnwJMPLel8P/BUbn8BcXglH/cunR0LUP7sflTxEz+Rv5qg+9yB8zgBsB4C0233WpcJxjeD6Dkq0EcoJObBR56F8dcb7NQtUDu7x6xxzcgSd7dHm5w==\""}
      ]
    }

    assert HTTPSignatures.validate_conn(conn, public_key)
  end

  test "it validates a conn and fetches the key" do
    conn = %{
      params: %{"actor" => "http://mastodon.example.org/users/admin"},
      req_headers: [
        {"host", "localtesting.pleroma.lol"},
        {"x-forwarded-for", "127.0.0.1"},
        {"connection", "close"},
        {"content-length", "2307"},
        {"user-agent", "http.rb/2.2.2 (Mastodon/2.1.0.rc3; +http://mastodon.example.org/)"},
        {"date", "Sun, 11 Feb 2018 17:12:01 GMT"},
        {"digest", "SHA-256=UXsAnMtR9c7mi1FOf6HRMtPgGI1yi2e9nqB/j4rZ99I="},
        {"content-type", "application/activity+json"},
        {"signature",
         "keyId=\"http://mastodon.example.org/users/admin#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) user-agent host date digest content-type\",signature=\"qXKqpQXUpC3d9bZi2ioEeAqP8nRMD021CzH1h6/w+LRk4Hj31ARJHDwQM+QwHltwaLDUepshMfz2WHSXAoLmzWtvv7xRwY+mRqe+NGk1GhxVZ/LSrO/Vp7rYfDpfdVtkn36LU7/Bzwxvvaa4ZWYltbFsRBL0oUrqsfmJFswNCQIG01BB52BAhGSCORHKtQyzo1IZHdxl8y80pzp/+FOK2SmHkqWkP9QbaU1qTZzckL01+7M5btMW48xs9zurEqC2sM5gdWMQSZyL6isTV5tmkTZrY8gUFPBJQZgihK44v3qgfWojYaOwM8ATpiv7NG8wKN/IX7clDLRMA8xqKRCOKw==\""},
        {"(request-target)", "post /users/demiurge/inbox"}
      ]
    }

    assert HTTPSignatures.validate_conn(conn)
  end

  test "validate this" do
    conn = %{
      params: %{"actor" => "https://niu.moe/users/rye"},
      req_headers: [
        {"x-forwarded-for", "149.202.73.191"},
        {"host", "testing.pleroma.lol"},
        {"x-cluster-client-ip", "149.202.73.191"},
        {"connection", "upgrade"},
        {"content-length", "2396"},
        {"user-agent", "http.rb/3.0.0 (Mastodon/2.2.0; +https://niu.moe/)"},
        {"date", "Sun, 18 Feb 2018 20:31:51 GMT"},
        {"digest", "SHA-256=dzH+vLyhxxALoe9RJdMl4hbEV9bGAZnSfddHQzeidTU="},
        {"content-type", "application/activity+json"},
        {"signature",
         "keyId=\"https://niu.moe/users/rye#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) user-agent host date digest content-type\",signature=\"wtxDg4kIpW7nsnUcVJhBk6SgJeDZOocr8yjsnpDRqE52lR47SH6X7G16r7L1AUJdlnbfx7oqcvomoIJoHB3ghP6kRnZW6MyTMZ2jPoi3g0iC5RDqv6oAmDSO14iw6U+cqZbb3P/odS5LkbThF0UNXcfenVNfsKosIJycFjhNQc54IPCDXYq/7SArEKJp8XwEgzmiC2MdxlkVIUSTQYfjM4EG533cwlZocw1mw72e5mm/owTa80BUZAr0OOuhoWARJV9btMb02ZyAF6SCSoGPTA37wHyfM1Dk88NHf7Z0Aov/Fl65dpRM+XyoxdkpkrhDfH9qAx4iuV2VEWddQDiXHA==\""},
        {"(request-target)", "post /inbox"}
      ]
    }

    assert HTTPSignatures.validate_conn(conn)
  end

  test "validate this too" do
    conn = %{
      params: %{"actor" => "https://niu.moe/users/rye"},
      req_headers: [
        {"x-forwarded-for", "149.202.73.191"},
        {"host", "testing.pleroma.lol"},
        {"x-cluster-client-ip", "149.202.73.191"},
        {"connection", "upgrade"},
        {"content-length", "2342"},
        {"user-agent", "http.rb/3.0.0 (Mastodon/2.2.0; +https://niu.moe/)"},
        {"date", "Sun, 18 Feb 2018 21:44:46 GMT"},
        {"digest", "SHA-256=vS8uDOJlyAu78cF3k5EzrvaU9iilHCX3chP37gs5sS8="},
        {"content-type", "application/activity+json"},
        {"signature",
         "keyId=\"https://niu.moe/users/rye#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) user-agent host date digest content-type\",signature=\"IN6fHD8pLiDEf35dOaRHzJKc1wBYh3/Yq0ItaNGxUSbJTd2xMjigZbcsVKzvgYYjglDDN+disGNeD+OBKwMqkXWaWe/lyMc9wHvCH5NMhpn/A7qGLY8yToSt4vh8ytSkZKO6B97yC+Nvy6Fz/yMbvKtFycIvSXCq417cMmY6f/aG+rtMUlTbKO5gXzC7SUgGJCtBPCh1xZzu5/w0pdqdjO46ePNeR6JyJSLLV4hfo3+p2n7SRraxM4ePVCUZqhwS9LPt3Zdhy3ut+IXCZgMVIZggQFM+zXLtcXY5HgFCsFQr5WQDu+YkhWciNWtKFnWfAsnsg5sC330lZ/0Z8Z91yA==\""},
        {"(request-target)", "post /inbox"}
      ]
    }

    assert HTTPSignatures.validate_conn(conn)
  end

  test "it generates a signature" do
    assert HTTPSignatures.sign(
             @private_key,
             "http://mastodon.example.org/users/admin#main-key",
             %{host: "mastodon.example.org"}
           ) =~ "keyId=\""
  end

  test "this too" do
    conn = %{
      params: %{"actor" => "https://mst3k.interlinked.me/users/luciferMysticus"},
      req_headers: [
        {"host", "soc.canned-death.us"},
        {"user-agent", "http.rb/3.0.0 (Mastodon/2.2.0; +https://mst3k.interlinked.me/)"},
        {"date", "Sun, 11 Mar 2018 12:19:36 GMT"},
        {"digest", "SHA-256=V7Hl6qDK2m8WzNsjzNYSBISi9VoIXLFlyjF/a5o1SOc="},
        {"content-type", "application/activity+json"},
        {"signature",
         "keyId=\"https://mst3k.interlinked.me/users/luciferMysticus#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) user-agent host date digest content-type\",signature=\"CTYdK5a6lYMxzmqjLOpvRRASoxo2Rqib2VrAvbR5HaTn80kiImj15pCpAyx8IZp53s0Fn/y8MjCTzp+absw8kxx0k2sQAXYs2iy6xhdDUe7iGzz+XLAEqLyZIZfecynaU2nb3Z2XnFDjhGjR1vj/JP7wiXpwp6o1dpDZj+KT2vxHtXuB9585V+sOHLwSB1cGDbAgTy0jx/2az2EGIKK2zkw1KJuAZm0DDMSZalp/30P8dl3qz7DV2EHdDNfaVtrs5BfbDOZ7t1hCcASllzAzgVGFl0BsrkzBfRMeUMRucr111ZG+c0BNOEtJYOHSyZsSSdNknElggCJekONYMYk5ZA==\""},
        {"x-forwarded-for", "2607:5300:203:2899::31:1337"},
        {"x-forwarded-host", "soc.canned-death.us"},
        {"x-forwarded-server", "soc.canned-death.us"},
        {"connection", "Keep-Alive"},
        {"content-length", "2006"},
        {"(request-target)", "post /inbox"}
      ]
    }

    assert HTTPSignatures.validate_conn(conn)
  end

  test "Signatures are stable on OTP26+ with undefined Map.keys/1 ordering" do
    user_ap_id = ~s|https://mastodon.social/users/lambadalambda#main-key|

    # credo:disable-for-lines:10 Credo.Check.Readability.MaxLineLength
    private_key =
      {:RSAPrivateKey, :"two-prime",
       28_756_005_415_572_484_042_763_333_825_843_542_309_845_812_712_410_557_401_080_975_665_944_065_546_687_053_162_744_064_118_733_269_231_397_398_437_337_112_118_822_093_392_471_512_223_279_218_516_015_781_047_498_524_701_011_344_834_660_702_300_339_866_912_903_829_480_780_711_421_965_266_995_321_160_936_070_443_834_148_409_010_341_833_400_730_167_696_056_399_186_720_243_079_891_586_296_037_178_998_225_766_461_225_833_387_132_735_728_367_079_742_073_397_917_419_922_243_432_508_105_117_797_420_565_462_233_617_076_268_056_263_255_047_301_378_690_482_484_074_947_841_896_587_287_731_635_988_553_127_288_143_474_145_525_724_255_965_068_001_976_777_796_779_533_346_344_982_614_532_834_052_163_179_471_788_571_859_959_462_813_779_224_935_806_760_043_776_072_659_926_191_283_296_091_970_506_062_030_984_091_470_929_266_003_011,
       65_537,
       25_233_957_481_263_615_321_105_929_907_140_426_297_194_450_296_637_489_603_371_636_030_768_012_109_037_222_140_003_526_671_473_370_973_612_835_255_371_123_456_268_345_987_778_455_955_579_105_800_632_735_218_909_015_602_715_449_920_523_322_539_825_529_794_789_191_349_004_359_581_262_836_335_214_000_723_765_372_612_446_022_890_653_506_246_486_594_506_922_860_631_830_586_989_403_926_452_005_709_205_303_322_232_716_957_756_646_692_607_039_707_869_476_684_696_402_780_134_400_419_892_792_178_031_077_409_586_563_911_414_056_442_023_360_496_531_178_932_232_960_305_935_443_131_268_902_698_561_476_776_016_479_175_692_849_949_462_150_713_017_077_076_502_024_471_442_627_225_940_981_940_235_409_308_119_456_929_193_383_984_043_313_294_569_986_326_805_396_849_905_695_401_341_785_041_176_269_677_759_870_702_155_638_035_216_833,
       177_783_479_898_101_225_090_255_510_574_083_286_496_133_004_386_710_828_810_208_011_585_370_522_423_888_313_371_324_368_555_961_532_882_416_417_904_776_985_934_672_514_976_013_258_017_209_904_688_467_121_759_107_569_488_391_607_820_963_070_360_845_953_724_899_435_077_154_851_205_162_368_630_793_919_412_726_847_267_327_206_524_298_869_094_621_709_415_910_252_626_752_919_831_010_415_748_000_011_171_076_295_507,
       161_747_342_509_294_677_375_914_222_076_718_822_400_941_009_725_673_497_592_218_662_627_801_812_486_196_781_966_854_364_060_860_604_565_261_653_867_622_254_133_771_945_941_570_294_029_162_561_824_060_451_304_312_080_304_336_296_778_308_854_167_291_375_658_502_471_854_680_814_450_977_871_287_689_807_234_161_418_665_899_011_960_362_411_939_434_739_945_865_837_893_017_653_193_120_275_492_634_544_890_342_133_073,
       123_805_792_744_090_236_231_352_842_624_633_216_540_229_399_990_922_616_477_243_136_560_183_183_131_724_655_293_252_252_262_470_488_414_492_620_912_707_582_908_471_838_976_307_567_979_117_793_613_942_520_560_353_851_471_393_328_796_571_914_616_150_395_685_653_681_393_507_030_443_145_175_121_546_663_534_758_084_477_982_611_022_208_463_716_823_171_582_964_249_502_302_157_684_475_706_308_237_675_051_295_467_455,
       43_886_525_847_998_503_945_839_856_828_481_836_213_643_179_195_598_305_296_013_431_479_127_391_086_402_355_569_138_109_796_454_266_603_284_903_628_088_849_398_152_688_446_724_796_197_973_185_747_828_599_800_010_336_328_664_846_259_546_333_289_634_484_977_333_276_691_333_662_550_426_301_283_819_829_290_902_213_203_488_353_612_145_267_697_743_695_099_217_027_471_712_771_550_117_715_775_974_296_614_737_687_265,
       90_283_918_084_263_940_362_515_004_232_322_731_054_469_960_381_679_586_824_438_807_054_374_237_285_157_945_298_098_405_267_918_780_650_461_768_149_536_719_331_445_672_754_509_549_303_377_662_269_991_550_734_109_336_424_702_266_640_645_630_618_719_389_738_627_119_442_677_159_263_798_411_854_553_104_387_445_904_992_891_045_248_551_150_818_831_926_648_119_173_927_771_126_688_710_466_061_199_827_928_052_600_203,
       :asn1_NOVALUE}

    headers = %{host: "test.test", "content-length": 100}

    expected_signature =
      ~s|keyId="https://mastodon.social/users/lambadalambda#main-key",algorithm="rsa-sha256",headers="content-length host",signature="sibUOoqsFfTDerquAkyprxzDjmJm6erYc42W5w1IyyxusWngSinq5ILTjaBxFvfarvc7ci1xAi+5gkBwtshRMWm7S+Uqix24Yg5EYafXRun9P25XVnYBEIH4XQ+wlnnzNIXQkU3PU9e6D8aajDZVp3hPJNeYt1gIPOA81bROI8/glzb1SAwQVGRbqUHHHKcwR8keiR/W2h7BwG3pVRy4JgnIZRSW7fQogKedDg02gzRXwUDFDk0pr2p3q6bUWHUXNV8cZIzlMK+v9NlyFbVYBTHctAR26GIAN6Hz0eV0mAQAePHDY1mXppbA8Gpp6hqaMuYfwifcXmcc+QFm4e+n3A=="|

    signature = HTTPSignatures.sign(private_key, user_ap_id, headers)

    assert signature == expected_signature
  end
end
